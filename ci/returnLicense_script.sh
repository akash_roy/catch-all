#!/usr/bin/env bash

set -x

echo "Trying to return unity licence"

${UNITY_EXECUTABLE:-xvfb-run --auto-servernum --server-args='-screen 0 640x480x24' unity-editor} \
  -logFile /dev/stdout \
  -batchmode \
  -quit \
  -nographics \
  -username "$UNITY_USERNAME" -password "$UNITY_PASSWORD" -returnlicense 

UNITY_EXIT_CODE=$?

if [ $UNITY_EXIT_CODE -eq 0 ]; then
  echo "License returned successfully, no failures occurred";
elif [ $UNITY_EXIT_CODE -eq 2 ]; then
  echo "License returned successfully, some failures occurred";
elif [ $UNITY_EXIT_CODE -eq 3 ]; then
  echo "Run failure (other failure)";
else
  echo "Unexpected exit code $UNITY_EXIT_CODE";
fi

exit $UNITY_EXIT_CODE
