﻿
namespace FrolicGamesAPI
{
  /// <summary>
  /// A global class to provide Remote API Variables.
  /// All the SetMethods are to be called/set Natively.
  /// All the Variables are to be used/get by Unity scripts.
  /// </summary>
  public class RemoteAPIVariablesProvider
  {
    #region Singleton Pattern
    private RemoteAPIVariablesProvider()
    {

    }

    private static RemoteAPIVariablesProvider instance;

    public static RemoteAPIVariablesProvider Instance
    {
      get
      {
        if (instance == null)
        {
          instance = new RemoteAPIVariablesProvider();
        }
        return instance;
      }
    }

    #endregion

    #region Variables (Get Properties)
    public string SessionId { get; private set; }
    public string GameBaseURL { get; private set; }
    #endregion

    #region SetMethods

    public void SetSessionId(string sessionId)
    {
      SessionId = sessionId;
    }

    public void SetGameBaseURL(string gameBaseURL)
    {
      GameBaseURL = gameBaseURL;
    }
    #endregion

  }
}