
namespace FrolicGamesAPI
{
  /// <summary>
  /// A global class to provide Local Player Information provider
  /// </summary>
  public class LocalPlayerInfoProvider
  {
    #region Singleton Pattern

    private LocalPlayerInfoProvider()
    {

    }

    private static LocalPlayerInfoProvider instance;

    public static LocalPlayerInfoProvider Instance
    {
      get
      {
        if (instance == null)
        {
          instance = new LocalPlayerInfoProvider();
        }
        return instance;
      }
    }
    #endregion

    #region Variables (Get Properties)
    public PlayerInfo PlayerInfo { get; private set; }
    #endregion

    #region SetMethods
    public void SetLocalPlayerInfo(PlayerInfo playerInfo)
    {
      PlayerInfo = playerInfo;
    }
    #endregion

  }
}
