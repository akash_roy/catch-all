﻿using System;
using System.Collections;
using UnityEngine;

namespace FrolicGamesAPI
{
  // A class that maintains game timer
  public class CountDownTimer : MonoBehaviour
  {
    public static CountDownTimer Instance { get; private set; }

    [Range(0, 5)]
    [SerializeField] int minute = 2;// minute for countdown timer to finish counting
    [Range(0, 60)]
    [SerializeField] int seconds;// seconds for countdown timer to finish counting

    WaitForSeconds waitOneSecond = new WaitForSeconds(1);// 1 sec delay

    IEnumerator timerRoutine;

    int currentTimeInSeconds;
    public int CurrentTimeInSeconds => currentTimeInSeconds;

    public event Action<string> getCurrentTime;// Event invoked to pass current timer time

    void Awake()
    {
      Instance = this;
    }

    // Method that starts the count down timer
    public void StartTimer()
    {
      if (NetworkManager.Instance.IsGameOver == true)
      {
        return;
      }

      if (timerRoutine != null)
      {
        StopCoroutine(timerRoutine);
      }
      int targetTimeInSeconds = (minute * 60) + seconds;
      currentTimeInSeconds = targetTimeInSeconds;
      timerRoutine = CountDownRoutine();
      StartCoroutine(timerRoutine);
    }

    // Method that stops the count down timer
    public void StopTimer()
    {
      if (timerRoutine != null)
      {
        StopCoroutine(timerRoutine);
      }
    }

    IEnumerator CountDownRoutine()
    {
      while (currentTimeInSeconds > 0)
      {
        yield return waitOneSecond;
        currentTimeInSeconds -= 1;

        float currentMinute = 0;
        float currentSecond = 0;

        if (currentTimeInSeconds > 0)
        {
          currentMinute = currentTimeInSeconds / 60;
          currentSecond = currentTimeInSeconds % 60;
        }

        getCurrentTime?.Invoke(string.Format("{0:0}:{1:00}", currentMinute, currentSecond));// Invoke currentTime event
      }

      NetworkManager.Instance.StopNetwork();// When time is up, stop the network
    }

    // Method that deducts game count down time with the time passed outside app
    // NOTE: To be invoked from Native App (via JS plugin)
    public void ChangeTime(int timePassed)
    {
      if (timerRoutine != null)
      {
        currentTimeInSeconds -= timePassed;
      }
    }

  }
}