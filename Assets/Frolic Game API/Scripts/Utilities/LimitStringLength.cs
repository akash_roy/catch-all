
namespace FrolicGamesAPI
{
  /// <summary>
  /// Classes to string length to maxLength limit
  /// </summary>
  public static class LimitStringLength
  {
    public static string LimitTo(this string data, int maxLength)
    {
      const string ellipses = "...";
      return (data == null || data.Length <= maxLength)
        ? data
        : $"{ data.Substring(0, maxLength - ellipses.Length) }{ellipses}";
    }
  }
}
