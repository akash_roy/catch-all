﻿using System;

/// <summary>
/// Classes to create Json Data variables used for server API calling
/// </summary>
namespace FrolicGamesAPI
{
  /// <summary>
  /// Class to create SessionData variable used to parse game session data and for server API calls
  /// </summary>
  [Serializable]
  public class SessionData
  {
    public string event_key;
    public string event_url;
    public string game_id;
    public string contest_id;
    public string session_id;
    public int game_mode;// if gameMode = 1 then game will be in Singleplayer mode/ if gameMode = 2 then game will be in Multiplayer mode
    public string game_base_url;// a game base url with a forward slash in the end
  }

  /// <summary>
  /// Class to create GameStopData variable used for server Game Stop API calls
  /// </summary>
  [Serializable]
  public class GameStopData
  {
    public int score;
    public string reason;

    public GameStopData(int _score, UserStatus user_status)
    {
      score = _score;
      reason = user_status.ToString();
    }
  }

  // Enum to specify User Status
  public enum UserStatus
  {
    UNKNOWN = 0,
    USER_DISCONNECTED = 5,
    GAME_OVER = 10,
    USER_EXITED = 15,
    GAME_TIMEOUT = 20,
    PENDING = 30,
  }

  /// <summary>
  /// Class to describe a player data to send/receive using Player-Data API calls
  /// </summary>
  [Serializable]
  public class PlayerData
  {
    public string status;// status of the player
    public int score;// current score of a player
    public int time;// time remaining in seconds of a player
  }

  /// <summary>
  /// Class to describe match data
  /// </summary>
  [Serializable]
  public class MatchData
  {
    public bool is_my_game_over;// a flag to know if a local player's game is over
    public string gamesession_started_at;// holds the server time, when all players should launch the gameplay scene
    public TopOpponent top_opponent;// Opponent with highest score
  }

  /// <summary>
  /// Class to describe opponents with highest score
  /// </summary>
  [Serializable]
  public class TopOpponent
  {
    public int score;// highest score among opponents
    public int count;// opponents count with highest score
    public PlayerInfo player;// opponent info with highest score
  }

  /// <summary>
  /// Class to describe player information
  /// </summary>
  [Serializable]
  public class PlayerInfo
  {
    public string id;// player's id
    public string username;// player's username
    public string avatar;// player's avatar
  }

  /// <summary>
  /// Class to create MessageData variable used for response messages from server API calls
  /// </summary>
  [Serializable]
  public class MessageData
  {
    public string message;
  }
}
