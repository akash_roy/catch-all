using UnityEngine;
using System.Runtime.InteropServices;

namespace FrolicGamesAPI
{
  // A class that communicates with JS to notify when gameplay is started and ended
  public class CommunicateGameplayStatusToJS : MonoBehaviour
  {
    void Start()
    {
      SendToJS_GameplayStarted();// To tell the browser that gameplay is started
    }

    #region Unity To JS communication

#if UNITY_WEBGL && !UNITY_EDITOR
    [DllImport("__Internal")]
    private static extern void SendGameplayStartedMessage(string message);


    [DllImport("__Internal")]
    private static extern void SendGameEndMessage(string message);
#endif
    //Tell the browser that gameplay is started
    public void SendToJS_GameplayStarted()
    {
      string MessageToSend = "Gameplay is started";
      Debug.Log("[RemoteAPICaller] : Sending Gameplay started message to JavaScript: " + MessageToSend);
#if UNITY_WEBGL && !UNITY_EDITOR
        SendGameplayStartedMessage(MessageToSend);// To tell the browser that gameplay is started
#endif
    }

    //Tell the Native App (via JS plugin) that a Game has Ended
    public void SendToJS_ToEndGame()
    {
      if (GameModeProvider.modeType == GameModeType.Multiplayer)
      {
        string MessageToSend = (NetworkManager.Instance.isLocalPlayerWinner == true) ? GAME_WON : GAME_END;
        Debug.Log("[RemoteAPICaller] : Sending Gameplay ended message to JavaScript: " + MessageToSend);
#if UNITY_WEBGL && !UNITY_EDITOR
        SendGameEndMessage(MessageToSend);
#endif
      }
    }

    #endregion

    private const string GAME_WON = "GAME_WON";
    private const string GAME_END = "GAME_END";
  }

}