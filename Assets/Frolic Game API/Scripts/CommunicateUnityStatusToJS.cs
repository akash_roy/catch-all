using UnityEngine;
using System.Runtime.InteropServices;
using UnityEngine.SceneManagement;

namespace FrolicGamesAPI
{
  // A class that communicates with JS to notify when Unity runtime is loaded and gameplay scene is launched
  public class CommunicateUnityStatusToJS : MonoBehaviour
  {

    void Awake()
    {
      Debug.Log("Awake CommunicateUnityStatusToJS");
    }

    #region Configurations
    // called first
    private void OnEnable()
    {
      Debug.Log("OnEnable CommunicateUnityStatusToJS");
      SceneManager.sceneLoaded += OnSceneLoaded;// Subscribe the OnSceneLoaded callback method to SceneManager.sceneLoaded Unity Event
    }

    // called when a scene is terminated
    void OnDisable()
    {
      SceneManager.sceneLoaded -= OnSceneLoaded;// Unsubscribe the OnSceneLoaded callback method from SceneManager.sceneLoaded Unity Event
    }

    // called second when a scene containing this(Bridge) script is loaded, which further calls SendToJS_SceneLoaded
    void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
      Debug.Log("OnSceneLoaded: " + scene.name);

      SendToJS_UnityLoaded();// To tell the Native App that a Unity scene is loaded
    }

    // called third
    void Start()
    {
#if UNITY_WEBGL && !UNITY_EDITOR
        WebGLInput.captureAllKeyboardInput = false;
#endif
    }

    #endregion

    #region Unity To JS communication

#if UNITY_WEBGL && !UNITY_EDITOR
    [DllImport("__Internal")]
    private static extern void SendUnityLoadedMessage(string message);
    [DllImport("__Internal")]
    private static extern void SendGameplaySceneLaunchedMessage(string message);
    [DllImport("__Internal")]
    private static extern void SendPlayerConnectedToGameMessage(string message);
    [DllImport("__Internal")]
    private static extern void SendConnectionStatus(int status);
#endif

    //Tell the Native App (via JS plugin) that Unity is loaded and ready to get API variables
    void SendToJS_UnityLoaded()
    {
      string MessageToSend = "Unity is Ready";
      Debug.Log("[Bridge] : Sending Unity Loaded message to JavaScript: " + MessageToSend);
#if UNITY_WEBGL && !UNITY_EDITOR
        SendUnityLoadedMessage(MessageToSend);
#endif
      FrolicEventHandler.Instance.OnNativeHandShake();
    }

    //Tell the Native App (via JS plugin) that a GameScene is launched
    public void SendToJS_GamePlaySceneLaunched()
    {
      string MessageToSend = "Gameplay scene has launched";
      Debug.Log("[Bridge] : Sending Gameplay Loaded message to JavaScript: " + MessageToSend);
#if UNITY_WEBGL && !UNITY_EDITOR
        SendGameplaySceneLaunchedMessage(MessageToSend);
#endif
      FrolicEventHandler.Instance.OnGameLaunch();
    }

    //Tell the Native App (via JS plugin) that local Player has connected to game
    public void SendToJS_PlayerHasConnectedToGame()
    {
      string MessageToSend = "Player has connected to game";
      Debug.Log("[Bridge] : Sending Player connected to game message to JavaScript: " + MessageToSend);
#if UNITY_WEBGL && !UNITY_EDITOR
        SendPlayerConnectedToGameMessage(MessageToSend);
#endif
      FrolicEventHandler.Instance.OnPongReceived();
    }

    int currentStatus = 0;
    //Tell the Native App (via JS plugin) that local Player has connected to game
    public void SendToJS_ConnectionStatus(int status)
    {
      if (currentStatus == status)
        return;

      currentStatus = status;
      Debug.Log("[Bridge] : Sending conneciton status : " + (status == 1 ? "Connected " : "Disconnected"));
#if UNITY_WEBGL && !UNITY_EDITOR
        SendConnectionStatus(status);
#endif
    }

    #endregion
  }

}
