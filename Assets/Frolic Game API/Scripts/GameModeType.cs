﻿
namespace FrolicGamesAPI
{
    // An enum for specifying the selected game mode (Singleplayer/ Multiplayer)
    public enum GameModeType
    {
        None = 0,
        Singleplayer = 1,// No API calls are made in this mode
        Multiplayer = 2
    }
}
