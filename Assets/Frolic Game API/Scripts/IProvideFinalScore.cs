
namespace FrolicGamesAPI
{
  public interface IProvideFinalScore
  {
      int GetLocalPlayerFinalScore();
  }
}
