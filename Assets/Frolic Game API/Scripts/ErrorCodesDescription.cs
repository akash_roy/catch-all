namespace FrolicGamesAPI
{
  // A class that maintains and describes all the error codes
  public static class ErrorCodesDescription
  {
    // Characters to append as prefix for each error code (so that Error Codes can be parsed from Computed Error String in Android side)
    public const string prefixSeperationCharacter = "-";

    // Error codes for missing api variables
    public const string game_mode = "1";
    public const string game_id = "2";
    public const string contest_id = "3";
    public const string session_id = "4";
    public const string game_base_url = "5";
    public const string event_key = "6";
    public const string event_url = "7";

    // Error codes for missing player info variables
    public const string playerInfo_id = "51";
    public const string playerInfo_username = "52";
    public const string playerInfo_avatar = "53";

    // Append error code with prefixEllipses prefix
    public static string AppendErrorCode(string errorCode)
    {
      return $"{prefixSeperationCharacter}{errorCode}";
    }

  }

}
