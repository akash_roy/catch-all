﻿﻿using System.Collections;
using System.Text;
using UnityEngine;
using UnityEngine.Networking;

namespace FrolicGamesAPI
{
  /// <summary>
  /// A class to perform all Remote API Calls to the server
  /// </summary>
  public class RemoteAPICaller : MonoBehaviour
  {
    public static RemoteAPICaller Instance { get; private set; } // singleton instance property
    [Header("Server URL")]
    [SerializeField] string url;// Server url

    [Space]
    [SerializeField] string sessionId;// Game Session id to be set/fetched by the native browser

    [Space]
    [SerializeField] float hearBeatTimeInSeconds = 1;// time between each heartbeat calls

    [Space]
    [Header("Whether to initialize this script through Natively(TRUE) OR Unity inspector(FALSE) for Server API Calls")]
    [SerializeField] bool shouldInitializeViaNatively = true;// Whether to initialize this script through Natively(TRUE) OR Unity inspector(FALSE)

    bool isInitialized = false;// flag to indicate all API variables are set inorder to make API calls
    [SerializeField] float dataTransmissionInterval = 1;//time in seconds to transmit data in intervals
    IProvideReceiveData provideReceiveData;// instance which will provide local player data and receive opponents data

    #region Initialization

    void Awake()
    {
      if (Instance == null)
      {
        Instance = this;
      }
      else if (Instance != null && Instance != this)// To have only a single instance of RemoteAPICaller class in gameplay scene
      {
        Destroy(gameObject);// Destroy other instance added/created in gamplay scene
        Debug.LogWarning($"[RemoteAPICaller] : Game can have only one RemoteAPICaller instance", this);
      }
    }

    // Begin the API calling system
    public void Begin(IProvideReceiveData _provideReceiveData, bool heartBeatInIntervals = false, bool sendDataInIntervals = false, bool receiveDataInIntervals = false)
    {
      provideReceiveData = _provideReceiveData;// initialize an IProvideReceiveData
      Initialization();// perform API variables initialization

      if (provideReceiveData == null)
      {
        isInitialized = false;
      }

      if (isInitialized == false)
      {
        Debug.LogWarning($"[RemoteAPICaller] didn't initialize for API calls", this);
        return;
      }

      if (heartBeatInIntervals == true)
      {
        InvokeRepeating("StartHeartBeat", 0.5f, hearBeatTimeInSeconds);// Start heartbeat for Ping Pong
      }

      if (sendDataInIntervals == true)
      {
        InvokeRepeating("SendData", 0.5f, dataTransmissionInterval);// Start sending local player data in intervals
      }
      else
      {
        SendData();// Send local player data in the beginning, just once for the first time
      }

      if (receiveDataInIntervals == true)
      {
        InvokeRepeating("ReceiveData", 0.5f, dataTransmissionInterval);// Start receiving all players data in intervals
      }

    }

    void Initialization()
    {
      PerformConfigurationForServerAPICalls();//Configure API variables for Server API calls
    }

    //Configuration for Server API calls by fetching API variables set via inspector or natively
    public void PerformConfigurationForServerAPICalls()
    {
      if (shouldInitializeViaNatively == true)
      {
        FetchVariablesSetViaNatively();
      }
      else
      {
        FetchVariablesSetFromUnityInspector();
      }
    }

    // Fetch API variable set via inspector
    private void FetchVariablesSetFromUnityInspector()
    {
      isInitialized = CheckInitializationIsSuccessful($"Using inspector values [RemoteAPICaller]", $"Make sure all inspector variables are filled [RemoteAPICaller]");
    }

    // Fetch API variables via RemoteAPIVariablesProvider which are set Natively
    private void FetchVariablesSetViaNatively()
    {
      sessionId = RemoteAPIVariablesProvider.Instance.SessionId;
      url = RemoteAPIVariablesProvider.Instance.GameBaseURL;// Set url to GameBaseURL

      isInitialized = CheckInitializationIsSuccessful($"Using native values [RemoteAPICaller]", $"Remote API Variables are not set [RemoteAPICaller]");
    }

    // Check if all API variables are set successfully
    bool CheckIfAllAPIVariablesAreSet()
    {
      return !string.IsNullOrEmpty(sessionId) && !string.IsNullOrEmpty(url);
    }

    bool CheckInitializationIsSuccessful(string logMessage, string warningMessage)
    {
      if (CheckIfAllAPIVariablesAreSet())
      {
        Debug.Log(logMessage, this);
        return true;
      }
      else
      {
        Debug.LogWarning(warningMessage, this);
        return false;
      }
    }

    #endregion

    #region HeartBeat

    void StartHeartBeat()
    {
      StartCoroutine(SendHeartBeatCoroutine());//Start HeartBeat coroutine
    }

    void StopHeartBeat()
    {
      CancelInvoke("StartHeartBeat");
    }

    IEnumerator SendHeartBeatCoroutine()
    {
      Debug.Log($"[RemoteAPICaller] : Heartbeat Started...", this);

      using (UnityWebRequest postRequest = new UnityWebRequest($"{url}game-sessions/{sessionId}/ping", "POST"))
      {
        postRequest.downloadHandler = new DownloadHandlerBuffer();

        yield return postRequest.SendWebRequest();

        if (postRequest.result == UnityWebRequest.Result.ConnectionError || postRequest.result == UnityWebRequest.Result.ProtocolError)
        {
          Debug.Log(postRequest.error);// Log Error message response
        }
        else
        {
          Debug.Log($"[RemoteAPICaller] : Heartbeat Response is {postRequest.downloadHandler.text}");
        }
      }

    }

    #endregion

    #region StopGame

    public void EndGame(GameStopData _gameStopData)
    {
      if (isInitialized == false)
      {
        Debug.LogWarning("[RemoteAPICaller] didn't initialize for API calls", this);
        return;
      }

      StopHeartBeat();// Stop Heart Beat
      StopSendingPlayerData();// Stop sending data
      StopReceivingPlayerData();// Stop receiving data
      StopGame(_gameStopData);// Stop Game by API Call
    }

    void StopGame(GameStopData _gameStopData)
    {
      if (_gameStopData != null)
      {
        StartCoroutine(SendGameStopCoroutine(_gameStopData));
      }
      else
      {
        Debug.Log($"[RemoteAPICaller] : GameStopData can't be empty...", this);
      }
    }

    IEnumerator SendGameStopCoroutine(GameStopData _gameStopData)
    {
      Debug.Log($"StopGame Started... [RemoteAPICaller]", this);

      GameStopData gameStopData = _gameStopData;
      string jsonData = JsonUtility.ToJson(gameStopData);// gameStopData in JSON String format

      using (UnityWebRequest postRequest = new UnityWebRequest($"{url}game-sessions/{sessionId}/stop", "POST"))
      {
        byte[] bodyRaw = Encoding.UTF8.GetBytes(jsonData);
        postRequest.uploadHandler = new UploadHandlerRaw(bodyRaw);
        postRequest.downloadHandler = new DownloadHandlerBuffer();

        postRequest.SetRequestHeader("Content-Type", "application/json");

        yield return postRequest.SendWebRequest();

        if (postRequest.result == UnityWebRequest.Result.ConnectionError || postRequest.result == UnityWebRequest.Result.ProtocolError)
        {
          Debug.Log(postRequest.error);// Log Error message response
        }
        else
        {
          Debug.Log($"[RemoteAPICaller] : StopGame Response is {postRequest.downloadHandler.text}");
        }
      }
    }

    #endregion

    #region SendData

    // Send local player data once
    public void SendData()
    {
      if (isInitialized == false)
      {
        Debug.LogWarning("[RemoteAPICaller] didn't initialize for API calls", this);
        return;
      }
      else if (NetworkManager.Instance.IsGameOver == true)
      {
        Debug.LogWarning("[RemoteAPICaller] Can't send data as game is over", this);
        return;
      }
      StartCoroutine(SendPlayerData());
    }

    void StopSendingPlayerData()
    {
      CancelInvoke("SendData");
    }

    // Send Local Player Data
    IEnumerator SendPlayerData()
    {
      Debug.Log($"Send Player Data... [RemoteAPICaller]", this);

      PlayerData playerData = provideReceiveData.ProvideLocalPlayerData();// PlayerData to send
      string jsonData = JsonUtility.ToJson(playerData);// playerData in JSON String format

      using (UnityWebRequest postRequest = new UnityWebRequest($"{url}game-sessions/{sessionId}/player-data", "PUT"))
      {
        byte[] bodyRaw = Encoding.UTF8.GetBytes(jsonData);
        postRequest.uploadHandler = new UploadHandlerRaw(bodyRaw);
        postRequest.downloadHandler = new DownloadHandlerBuffer();

        postRequest.SetRequestHeader("Content-Type", "application/json");

        yield return postRequest.SendWebRequest();

        if (postRequest.result == UnityWebRequest.Result.ConnectionError || postRequest.result == UnityWebRequest.Result.ProtocolError)
        {
          Debug.Log(postRequest.error);// Log Error message response
        }
        else
        {
          Debug.Log($"[RemoteAPICaller] : SendData Response is {postRequest.downloadHandler.text}");
        }
      }

    }

    #endregion

    #region ReceiveData

    // Receive opponents/players data once
    public void ReceiveData()
    {
      if (isInitialized == false)
      {
        Debug.LogWarning("[RemoteAPICaller] didn't initialize for API calls", this);
        return;
      }
      else if (NetworkManager.Instance.IsGameOver == true)
      {
        Debug.LogWarning("[RemoteAPICaller] Can't receive data as game is over", this);
        return;
      }
      StartCoroutine(ReceivePlayerData());
    }

    void StopReceivingPlayerData()
    {
      CancelInvoke("ReceiveData");
    }

    // Receive all players data
    IEnumerator ReceivePlayerData()
    {
      Debug.Log($"Receive Players Data... [RemoteAPICaller]", this);

      using (UnityWebRequest postRequest = new UnityWebRequest($"{url}game-sessions/{sessionId}/player-data", "GET"))
      {
        postRequest.downloadHandler = new DownloadHandlerBuffer();

        yield return postRequest.SendWebRequest();

        if (postRequest.result == UnityWebRequest.Result.ConnectionError || postRequest.result == UnityWebRequest.Result.ProtocolError)
        {
          Debug.Log(postRequest.error);// Log Error message response
        }
        else
        {
          Debug.Log($"[RemoteAPICaller] : Received data Response is {postRequest.downloadHandler.text}");

          provideReceiveData.ReceiveMatchData(JsonUtility.FromJson<MatchData>(postRequest.downloadHandler.text));// Received Match Data
        }
      }

    }
    #endregion

  }

}