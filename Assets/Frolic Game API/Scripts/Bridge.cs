﻿using UnityEngine;
using System.Runtime.InteropServices;
using UnityEngine.SceneManagement;
using System.Collections;
using UnityEngine.Networking;
using System;

namespace FrolicGamesAPI
{
  // Note: This script should be in the very first Unity scene where you want to initialize/set API Variables
  // This class acts as a bridge between Unity and Native App, and has necessary methods to initialize/set API Variables
  [RequireComponent(typeof(CommunicateUnityStatusToJS))]
  public class Bridge : MonoBehaviour
  {
    [Header("Scene to load when API variables are set successfully")]
    public string sceneToLoad;// A GAME SCENE to be launched
    string url;// Server url
    bool sceneToLoadAlreadyBeingLaunched = false;
    bool playerHasConnectedToGame = false;// set when receiving first "PONG" message from server

    CommunicateUnityStatusToJS communicateUnityStatusToJS;

    int apiVariableRequestCount = 0;// number of times api variable are requested
    const int maxRequestCountForVariables = 2;// maximum number of times api variable can be requested
    bool isGameTerminated = false;// Is game terminated due to mission API variables
    int playerVariableRequestCount = 0;// number of times player info variable are requested

#if UNITY_WEBGL && !UNITY_EDITOR
    [DllImport("__Internal")]
    private static extern void SendAPIVariablesErrorCode(string errorCode);
    [DllImport("__Internal")]
    private static extern void SendPlayerInfoErrorCode(string errorCode);
#endif

    //Tell the Native App (via JS plugin) that API variables are missing
    void SendToJS_APIVariablesErrorCode(string errorCode)
    {
      Debug.Log("[Bridge] : Send API Variables ErrorCode: " + errorCode);
#if UNITY_WEBGL && !UNITY_EDITOR
        SendAPIVariablesErrorCode(errorCode);
#endif
    }

    //Tell the Native App (via JS plugin) that Player info variables are missing
    void SendToJS_PlayerInfoErrorCode(string errorCode)
    {
      Debug.Log("[Bridge] : Send Player info Variables ErrorCode: " + errorCode);
#if UNITY_WEBGL && !UNITY_EDITOR
        SendPlayerInfoErrorCode(errorCode);
#endif
    }

    void Awake()
    {
      Debug.Log("Awake Bridge");
      communicateUnityStatusToJS = GetComponent<CommunicateUnityStatusToJS>();
    }

    #region Methods to Set API Varibles from Native

    /// <summary>
    /// Method to initialise all API variables in a single call, which is called from browser(WebGL builds)
    /// </summary>
    /// <param name="API_Variables">API_Varibles should be a JSON String in the format: "{"game_id" : "YOUR VALUE","contest_id" : "YOUR VALUE","session_id" : "YOUR VALUE","authToken" : "YOUR VALUE"}</param>
    public void Send_APIVariables_ToUnity(string API_Variables)
    {
      SessionData sessionData = null;

      try
      {
        sessionData = JsonUtility.FromJson<SessionData>(API_Variables);

        GameModeProvider.modeType = (GameModeType)sessionData.game_mode;// set the game mode of GameModeProvider( if gameMode = 1 then Singleplayer/ if gameMode = 2 then Multiplayer)
        if (GameModeProvider.modeType == GameModeType.None)
        {
          Debug.LogWarning($"[Bridge] : GameModeType shouldn't be None", this);// just a warning for debugging
          ComputeAPI_VariablesErrorCode(sessionData);// Send error code
          return;
        }

        if (GameModeProvider.modeType == GameModeType.Singleplayer)// No API calls are made in this mode
        {
          LaunchSceneToLoad();// Launch sceneToLoad scene
          Debug.LogWarning($"[Bridge] : Game launced in Singleplayer mode", this);// just a warning for debugging
          return;
        }

        bool isSessionDataValid = !string.IsNullOrEmpty(sessionData.game_id) && !string.IsNullOrEmpty(sessionData.contest_id) && !string.IsNullOrEmpty(sessionData.session_id) && !string.IsNullOrEmpty(sessionData.game_base_url);
        bool isGameAnalyticsDataValid = !string.IsNullOrEmpty(sessionData.event_key) && !string.IsNullOrEmpty(sessionData.event_url);

        if (isSessionDataValid == true && isGameAnalyticsDataValid == true)
        {
          RemoteAPIVariablesProvider.Instance.SetSessionId(sessionData.session_id);// Set sessionID
          RemoteAPIVariablesProvider.Instance.SetGameBaseURL(sessionData.game_base_url);// Set gameBaseURL

          UserInfoData.pInstance.userInfo.gameId = sessionData.game_id;
          UserInfoData.pInstance.userInfo.contestId = sessionData.contest_id;
          UserInfoData.pInstance.userInfo.gameMode = sessionData.game_mode.ToString();
          UserInfoData.pInstance.userInfo.sessionId = sessionData.session_id;
          url = sessionData.game_base_url;// for API calls from Bridge script

          Debug.Log($"[Bridge] : All API variables are set properly, so multiplayer mode loading is initiated");
          InvokeRepeating("StartHeartBeat", 0.5f, 1);// Start heartbeat for Ping Pong every 1 second

          GameAnalytics.OnEventInitialize(sessionData.event_key, sessionData.event_url); // Analytics inilialize 
          FrolicEventHandler.Instance.OnPingStart(); // Ping event triggered

          switch (GameModeProvider.modeType)
          {
            case GameModeType.Multiplayer:
              Debug.LogWarning($"[Bridge] : Game launced in Multiplayer mode", this);// just a warning for debugging
              CheckIfAllPlayersAreReady();// If all connected players are ready, then launch the sceneToLoad scene
              break;
          }
        }
        else
        {
          Debug.Log($"[Bridge] : API variables are not set properly, so multiplayer mode loading didn't commence");

          ComputeAPI_VariablesErrorCode(sessionData);// Send error code
        }
      }
      catch (Exception ex)
      {
        Debug.LogError($"[Bridge] : API variables are not set properly, so multiplayer mode loading didn't commence : { ex.Message}", this);
        ComputeAPI_VariablesErrorCode(sessionData);// Send error code
      }

    }

    private void ComputeAPI_VariablesErrorCode(SessionData sessionData)
    {
      apiVariableRequestCount += 1;

      if (apiVariableRequestCount >= maxRequestCountForVariables)
      {
        if (isGameTerminated == true)
        {
          return;
        }
        SendToJS_APIVariablesErrorCode("2999");// Send termination error code
        isGameTerminated = true;
        return;
      }

      string computedErrorCode = "";
      if (sessionData.game_mode == 0)
      {
        computedErrorCode += ErrorCodesDescription.AppendErrorCode(ErrorCodesDescription.game_mode);
      }
      if (string.IsNullOrEmpty(sessionData.game_id))
      {
        computedErrorCode += ErrorCodesDescription.AppendErrorCode(ErrorCodesDescription.game_id);
      }
      if (string.IsNullOrEmpty(sessionData.contest_id))
      {
        computedErrorCode += ErrorCodesDescription.AppendErrorCode(ErrorCodesDescription.contest_id);
      }
      if (string.IsNullOrEmpty(sessionData.session_id))
      {
        computedErrorCode += ErrorCodesDescription.AppendErrorCode(ErrorCodesDescription.session_id);
      }
      if (string.IsNullOrEmpty(sessionData.game_base_url))
      {
        computedErrorCode += ErrorCodesDescription.AppendErrorCode(ErrorCodesDescription.game_base_url);
      }
      if (string.IsNullOrEmpty(sessionData.event_key))
      {
        computedErrorCode += ErrorCodesDescription.AppendErrorCode(ErrorCodesDescription.event_key);
      }
      if (string.IsNullOrEmpty(sessionData.event_url))
      {
        computedErrorCode += ErrorCodesDescription.AppendErrorCode(ErrorCodesDescription.event_url);
      }

      // Send API Variables Error Code
      SendToJS_APIVariablesErrorCode(computedErrorCode.Substring(1, computedErrorCode.Length - 1));// Send error code (first '-' character removed)
    }

    public void Send_PlayerInfoVariables_ToUnity(string PLAYER_INFO)
    {
      PlayerInfo localplayerInfo = JsonUtility.FromJson<PlayerInfo>(PLAYER_INFO);
      bool isPlayerInfoValid = !string.IsNullOrEmpty(localplayerInfo.id) && !string.IsNullOrEmpty(localplayerInfo.username) && !string.IsNullOrEmpty(localplayerInfo.avatar);
      LocalPlayerInfoProvider.Instance.SetLocalPlayerInfo(localplayerInfo);// Set localplayerInfo

      if (isPlayerInfoValid == true)
      {
        Debug.LogWarning($"[Bridge] : Local Player Info {PLAYER_INFO}", this);// log player info
        UserInfoData.pInstance.userInfo.id = localplayerInfo.id;// Set userId for Game analytics
      }
      else
      {
        playerVariableRequestCount += 1;

        if (playerVariableRequestCount >= maxRequestCountForVariables)
        {
          return;
        }

        string computedErrorCode = "";
        if (string.IsNullOrEmpty(localplayerInfo.id))
        {
          computedErrorCode += ErrorCodesDescription.AppendErrorCode(ErrorCodesDescription.playerInfo_id);
        }
        if (string.IsNullOrEmpty(localplayerInfo.username))
        {
          computedErrorCode += ErrorCodesDescription.AppendErrorCode(ErrorCodesDescription.playerInfo_username);
        }
        if (string.IsNullOrEmpty(localplayerInfo.avatar))
        {
          computedErrorCode += ErrorCodesDescription.AppendErrorCode(ErrorCodesDescription.playerInfo_avatar);
        }

        // Send Player Info Error Code
        SendToJS_PlayerInfoErrorCode(computedErrorCode.Substring(1, computedErrorCode.Length - 1));// Send error code (first '-' character removed)
      }

    }

    // Check if all players are ready
    void CheckIfAllPlayersAreReady()
    {
      InvokeRepeating("AreAllPlayersReady", 1, 1);// Check if all players are ready every 1 sec
    }

    void AreAllPlayersReady()
    {
      StartCoroutine(ReceivePlayerAreReady());
    }

    // Receive Players are ready from server
    IEnumerator ReceivePlayerAreReady()
    {
      Debug.Log($"[Bridge] : Check if all Players Ready...", this);

      // If API variables are not initialized, don't make ReceveData API call
      if (string.IsNullOrEmpty(RemoteAPIVariablesProvider.Instance.SessionId) || string.IsNullOrEmpty(url))
      {
        yield break;
      }

      using (UnityWebRequest postRequest = new UnityWebRequest($"{url}game-sessions/{RemoteAPIVariablesProvider.Instance.SessionId}/player-data", "GET"))
      {
        postRequest.downloadHandler = new DownloadHandlerBuffer();

        yield return postRequest.SendWebRequest();

        if (postRequest.result == UnityWebRequest.Result.ConnectionError || postRequest.result == UnityWebRequest.Result.ProtocolError)
        {
          Debug.Log($"[Bridge] : {postRequest.error}");
        }
        else
        {
          Debug.Log($"[Bridge] : ReceivePlayerAreReady Response is {postRequest.downloadHandler.text}");

          if (sceneToLoadAlreadyBeingLaunched == false)
          {
            MatchData matchData = JsonUtility.FromJson<MatchData>(postRequest.downloadHandler.text);// Receive Players Ready info

            // If local player didn't disconnect from server
            if (!string.IsNullOrEmpty(matchData.gamesession_started_at))// If allPlayers.gamesession_started_at string has a time value, Launch SceneToLoad
            {
              sceneToLoadAlreadyBeingLaunched = true;
              Debug.Log($"[Bridge] : Players are Ready at {matchData.gamesession_started_at}");
              LaunchSceneToLoad();// Launch sceneToLoad scene
            }
          }
        }
      }

    }

    // Launch the sceneToLoad scene when server says all players are ready
    void LaunchSceneToLoad()
    {
      SceneManager.LoadScene($"{sceneToLoad}");// Load sceneToLoad (set in inspector) after all variables are set successfully
      communicateUnityStatusToJS.SendToJS_GamePlaySceneLaunched();// Tell native App (via JS plugin), that gameplay scene is launched
    }

    #endregion

    #region HeartBeat

    void StartHeartBeat()
    {
      StartCoroutine(SendHeartBeatCoroutine());//Start HeartBeat coroutine
    }

    // Send heartbeat to tell the server that local player is ready
    IEnumerator SendHeartBeatCoroutine()
    {
      // If API variables are not initialized, don't make Heartbeat API call
      if (string.IsNullOrEmpty(RemoteAPIVariablesProvider.Instance.SessionId))
      {
        yield break;
      }

      Debug.Log($"[Bridge] : Heartbeat Started... [RemoteAPICaller]", this);

      using (UnityWebRequest postRequest = new UnityWebRequest($"{url}game-sessions/{RemoteAPIVariablesProvider.Instance.SessionId}/ping", "POST"))
      {
        postRequest.downloadHandler = new DownloadHandlerBuffer();

        yield return postRequest.SendWebRequest();

        if (postRequest.result == UnityWebRequest.Result.ConnectionError || postRequest.result == UnityWebRequest.Result.ProtocolError)
        {
          communicateUnityStatusToJS.SendToJS_ConnectionStatus(0);
          Debug.Log(postRequest.error);
        }
        else
        {
          if (playerHasConnectedToGame == false)
          {
            MessageData messageData = JsonUtility.FromJson<MessageData>(postRequest.downloadHandler.text);
            if (messageData.message == "PONG")
            {
              playerHasConnectedToGame = true;
              communicateUnityStatusToJS.SendToJS_PlayerHasConnectedToGame();
            }
          }
          Debug.Log($"[Bridge] : Heartbeat Response is {postRequest.downloadHandler.text}");
          communicateUnityStatusToJS.SendToJS_ConnectionStatus(1);
        }
      }
    }

    #endregion
  }
}
