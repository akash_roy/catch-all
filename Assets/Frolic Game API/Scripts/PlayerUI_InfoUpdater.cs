using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Networking;

namespace FrolicGamesAPI
{
  // A class to update local player and current opponent player Username and Avatar in UI
  public class PlayerUI_InfoUpdater : MonoBehaviour
  {
    public static PlayerUI_InfoUpdater Instance { get; private set; } // singleton instance property

    PlayerInfo localPlayerInfo;
    public PlayerInfo LocalPlayerInfo => localPlayerInfo;// Get property for localPlayerInfo
    PlayerInfo currentOpponentPlayerInfo;
    public PlayerInfo OpponentPlayerInfo => currentOpponentPlayerInfo;// Get property for opponentPlayerInfo

    [SerializeField] Sprite dummyAvatarSprite;
    Dictionary<string, Sprite> playerAvatarCollection = new Dictionary<string, Sprite>();// Key is player id and value is player avatar sprite

    IEnumerator loadOpponentAvatarRoutine;

    public event Action<string> getLocalPlayer_Name;// Event invoked to get local player name
    public event Action<Sprite> getLocalPlayer_Avatar;// Event invoked to get local player avatar
    public event Action<string> getOpponentPlayer_Name;// Event invoked to get opponent player name
    public event Action<Sprite> getOpponentPlayer_Avatar;// Event invoked to get opponent player avatar
    public event Action<int> getOpponentScore;// Event invoked on receiving opponent score

    const int imageSize = 100;// image pixel size for player avatar
    const int usernameLimit = 10;// character length limit for username
    const string dummyLocalPlayerName = "You";// dummy local player username

    void Awake()
    {
      localPlayerInfo = LocalPlayerInfoProvider.Instance.PlayerInfo;// Set local player info
      if (Instance == null)
      {
        Instance = this;
      }
      else if (Instance != null && Instance != this)// To have only a single instance of PlayerUI_InfoUpdater class in gameplay scene
      {
        Destroy(gameObject);// Destroy other instance added/created in gamplay scene
        Debug.LogWarning($"[PlayerUI_InfoUpdater] : Game can have only one PlayerUI_InfoUpdater instance", this);
      }
    }

    void Start()
    {
      StartCoroutine(LoadLocalPlayerAvatar());
    }

    public void UpdateOpponentInfo(PlayerInfo _opponentPlayerInfo = null)// When UpdateOpponentInfo is called with default parameter value is used, opponent UI is updated with latest current opponent player info
    {
      if (_opponentPlayerInfo != null)
      {
        currentOpponentPlayerInfo = _opponentPlayerInfo;
        getOpponentPlayer_Name?.Invoke(_opponentPlayerInfo.username.LimitTo(usernameLimit));// Invoke getOpponentPlayerName event with character limit to parameter
        if (playerAvatarCollection.ContainsKey(_opponentPlayerInfo.id))
        {
          getOpponentPlayer_Avatar?.Invoke(playerAvatarCollection[_opponentPlayerInfo.id]);// Set opponent avatar
        }
        else
        {
          loadOpponentAvatarRoutine = LoadOpponentPlayerAvatar(_opponentPlayerInfo);
          StartCoroutine(loadOpponentAvatarRoutine);
        }
      }
      else if (currentOpponentPlayerInfo != null)
      {
        getOpponentPlayer_Name?.Invoke(currentOpponentPlayerInfo.username.LimitTo(usernameLimit));// Invoke getOpponentPlayerName event with character limit to parameter
        if (playerAvatarCollection.ContainsKey(currentOpponentPlayerInfo.id))
        {
          getOpponentPlayer_Avatar?.Invoke(playerAvatarCollection[currentOpponentPlayerInfo.id]);// Set opponent avatar
        }
      }
    }

    public void UpdateOpponentScore(int enemyScore)
    {
      getOpponentScore?.Invoke(enemyScore);// Invoke getEnemyScore event
    }

    IEnumerator LoadLocalPlayerAvatar()
    {
      yield return new WaitForSeconds(1.5f);// A delay for event subscribers/listeners to setup

      // If localPlayerInfo is not available
      if (localPlayerInfo == null)
      {
        yield break;
      }

      // If local player username has a valid value
      if (!string.IsNullOrEmpty(localPlayerInfo.username))
      {
        getLocalPlayer_Name?.Invoke(localPlayerInfo.username.LimitTo(usernameLimit));// Invoke getLocalPlayer_Name event with character limit to parameter
      }
      else
      {
        getLocalPlayer_Name?.Invoke($"{dummyLocalPlayerName}".LimitTo(usernameLimit));// Invoke getLocalPlayer_Name event with dummy name character limit to parameter
      }

      if (dummyAvatarSprite != null)
      {
        getLocalPlayer_Avatar?.Invoke(dummyAvatarSprite);// Set dummy sprite as avatar while player avatar is being loaded
      }
      else
      {
        Debug.LogWarning($"[PlayerInfoMaker] : Dummy avatar sprite missing");
      }

      Debug.Log($"[PlayerInfoMaker] : Fetching local player avatar...", this);

      // If player avatar value is missing don't load local player avatar
      if (string.IsNullOrEmpty(localPlayerInfo.avatar))
      {
        yield break;
      }

      string imageURL = localPlayerInfo.avatar;
      imageURL = imageURL.Replace("original", $"resize-w:{imageSize}");

      using (UnityWebRequest getTextureRequest = UnityWebRequestTexture.GetTexture($"{imageURL}"))
      {
        yield return getTextureRequest.SendWebRequest();

        if (getTextureRequest.result == UnityWebRequest.Result.ConnectionError || getTextureRequest.result == UnityWebRequest.Result.ProtocolError)
        {
          Debug.Log($"[PlayerInfoMaker] : {getTextureRequest.error}");
        }
        else
        {
          // Get downloaded texture
          Texture2D downloadedTexture = DownloadHandlerTexture.GetContent(getTextureRequest);

          Rect rect = new Rect(0, 0, downloadedTexture.width, downloadedTexture.height);
          Sprite sprite = Sprite.Create(downloadedTexture, rect, new Vector2(0.5f, 0.5f), 100);// Create a sprite from downloaded texture

          if (playerAvatarCollection.ContainsKey(localPlayerInfo.id))
          {
            playerAvatarCollection.Add(localPlayerInfo.id, sprite);// Add local player avatar to playerAvatarCollection
          }

          getLocalPlayer_Avatar?.Invoke(sprite);// Update local player avatar in UI
          Debug.Log($"[PlayerInfoMaker] : Local player avatar set");
        }
      }

    }

    IEnumerator LoadOpponentPlayerAvatar(PlayerInfo opponetPlayerInfo)
    {
      if (dummyAvatarSprite != null)
      {
        getOpponentPlayer_Avatar?.Invoke(dummyAvatarSprite);// Set dummy sprite as avatar while opponent avatar is being loaded
      }
      else
      {
        Debug.LogWarning($"[PlayerInfoMaker] : Dummy avatar sprite missing");
      }

      Debug.Log($"[PlayerInfoMaker] : Fetching opponent player avatar...", this);

      string imageURL = opponetPlayerInfo.avatar;
      imageURL = imageURL.Replace("original", $"resize-w:{imageSize}");

      using (UnityWebRequest getTextureRequest = UnityWebRequestTexture.GetTexture($"{imageURL}"))
      {
        yield return getTextureRequest.SendWebRequest();

        if (getTextureRequest.result == UnityWebRequest.Result.ConnectionError || getTextureRequest.result == UnityWebRequest.Result.ProtocolError)
        {
          Debug.Log($"[PlayerInfoMaker] : {getTextureRequest.error}");
        }
        else
        {
          // Get downloaded texture
          Texture2D downloadedTexture = DownloadHandlerTexture.GetContent(getTextureRequest);

          Rect rect = new Rect(0, 0, downloadedTexture.width, downloadedTexture.height);
          Sprite sprite = Sprite.Create(downloadedTexture, rect, new Vector2(0.5f, 0.5f), 100);// Create a sprite from downloaded texture

          if (playerAvatarCollection.ContainsKey(opponetPlayerInfo.id))
          {
            playerAvatarCollection.Add(opponetPlayerInfo.id, sprite);// Add opponent player avatar to playerAvatarCollection
            UpdateOpponentInfo();// Set opponent UI with latest current opponent player info
          }
          Debug.Log($"[PlayerInfoMaker] : Opponent player avatar set");
        }
      }

    }

  }

}