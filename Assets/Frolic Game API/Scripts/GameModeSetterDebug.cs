﻿using UnityEngine;

namespace FrolicGamesAPI
{
    public class GameModeSetterDebug : MonoBehaviour
    {
#if UNITY_EDITOR
        public GameModeType modeType = GameModeType.Singleplayer;
        void Awake()
        {
            if(modeType == GameModeType.Singleplayer)
            {
                GameModeProvider.modeType = GameModeType.Singleplayer;
            }
            else if(modeType == GameModeType.Multiplayer)
            {
                GameModeProvider.modeType = GameModeType.Multiplayer;
            }
        }
#endif
    }
}
