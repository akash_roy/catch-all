using UnityEngine;

namespace FrolicGamesAPI
{
  [System.Serializable]
  public struct UserInfo
  {
    public string id;
    public string gameId;
    public string sessionId;
    public string contestId;
    public string gameMode;
    public string score;
  }

  public class UserInfoData : MonoBehaviour
  {
    private static UserInfoData instance;
    public static UserInfoData pInstance { get { return instance; } private set { instance = value; } }
    public UserInfo userInfo = new UserInfo();

    private void Awake()
    {
      Debug.Log("Awake UserInfoData");
      if (pInstance == null)
        pInstance = this;
      else
        Destroy(this.gameObject);
      DontDestroyOnLoad(this.gameObject);
    }
  }
}

