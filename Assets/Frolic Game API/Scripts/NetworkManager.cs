using System;
using UnityEngine;

namespace FrolicGamesAPI
{
  // A class that manages gameplay
  public class NetworkManager : MonoBehaviour, IProvideReceiveData
  {
    public static NetworkManager Instance { get; private set; } // singleton instance property
    CountDownTimer countDownTimer;
    RemoteAPICaller remoteAPICaller;
    CommunicateGameplayStatusToJS communicateGamePlayStatusToJS;
    PlayerUI_InfoUpdater playerUI_InfoUpdater;

    public event Action onGameOver;// Event invoked on Game Over

    public bool IsGameOver => isGameOver;// Indicates that game is over for local player
    bool isGameOver;
    public int LocalPlayerScore => localPlayerScore;// Get property for local player score
    int localPlayerScore;
    public int EnemyScore => enemyScore;// Get property for remote opponent score
    int enemyScore;

    const int usernameLimit = 10;// character length limit for username
    public string EnemyName => enemyName.LimitTo(usernameLimit);// Get property for remote opponent name
    string enemyName;
    public int EnemiesWithHighScoreCount => enemiesWithHighScoreCount;// Get property for remote opponent's count with same score
    int enemiesWithHighScoreCount;// remote opponent's count with same score

    public bool isLocalPlayerWinner { get; private set; }// Indicates whether local player won by crossing opponent score
    IProvideFinalScore provideFinalScore;// Used to set final score when game is over

    void Awake()
    {
      Initialize();// Initialize NetworkManager
    }

    // Initialize NetworkManager
    void Initialize()
    {
      if (Instance == null)
      {
        Instance = this;
      }
      else if (Instance != null && Instance != this)// To have only a single instance of NetworkManager class in gameplay scene
      {
        Destroy(gameObject);// Destroy other instance added/created in gamplay scene
        Debug.LogWarning($"[NetworkManager] : Game can have only one NetworkManager instance", this);
      }

      countDownTimer = GetComponent<CountDownTimer>();
      remoteAPICaller = GetComponent<RemoteAPICaller>();
      playerUI_InfoUpdater = GetComponent<PlayerUI_InfoUpdater>();
      communicateGamePlayStatusToJS = GetComponent<CommunicateGameplayStatusToJS>();
    }

    // Call this method to start the game for local player
    public void StartNetwork(IProvideFinalScore _provideFinalScore)
    {
      provideFinalScore = _provideFinalScore;

      // Begin remoteAPICaller based on GameModeType
      switch (GameModeProvider.modeType)
      {
        case GameModeType.Singleplayer:
          // No API calls are made in this mode
          break;
        case GameModeType.Multiplayer:
          if (provideFinalScore == null)
          {
            Debug.LogWarning($"[NetworkManager] : provideFinalScore field didn't initialize", this);
            return;
          }
          remoteAPICaller.Begin(this, true, true, true);// Begin remote API calling, with receive API call
          break;
      }

      localPlayerScore = 0;
      isGameOver = false;
      countDownTimer.StartTimer();
    }

    // Call this method to stop the game for local player
    public void StopNetwork()
    {
      if (isGameOver == true)
      {
        return;
      }

      countDownTimer.StopTimer();

      if (GameModeProvider.modeType == GameModeType.Multiplayer)// If we are playing in Multiplayer mode
      {
        if (provideFinalScore != null)
        {
          localPlayerScore = provideFinalScore.GetLocalPlayerFinalScore();// Set localplayer final score
        }
        //Creating gameStopData for Stop API call
        GameStopData gameStopData = new GameStopData(localPlayerScore, UserStatus.GAME_OVER);// Final score with reason to stop the game
        remoteAPICaller.EndGame(gameStopData);// perform stop API call
      }

      isGameOver = true;
      onGameOver?.Invoke();// Invoke onGameOver event
      Invoke("RedirectForResult", 0.5f);
    }

    // To add score and send local player data at particular point in game
    public void SubmitScore(int scoreToAdd)
    {
      if (isGameOver == false)
      {
        localPlayerScore = scoreToAdd;
        if (GameModeProvider.modeType == GameModeType.Multiplayer)// If we are in Multiplayer mode
        {
          remoteAPICaller.SendData();// Send player data when we lose all our lives
        }
      }
    }

    // Provides local player data to be send to server
    public PlayerData ProvideLocalPlayerData()
    {
      PlayerData playerData = new PlayerData();
      playerData.score = localPlayerScore;// send local player score
      playerData.time = countDownTimer.CurrentTimeInSeconds;// send local player time

      return playerData;
    }

    // Receives match data
    public void ReceiveMatchData(MatchData matchData)
    {
      if (isGameOver == true)
      {
        return;
      }

      TopOpponent top_opponent = matchData.top_opponent;

      enemyScore = top_opponent.score;
      if (enemyScore > 0)
      {
        playerUI_InfoUpdater.UpdateOpponentScore(enemyScore);// Delegate playerUI_InfoUpdater to notify opponent score update
      }

      if (enemiesWithHighScoreCount > 0)
      {
        enemiesWithHighScoreCount = top_opponent.count;// Set enemy count with same score
      }

      PlayerInfo newOpponentPlayerInfo = top_opponent.player;// Get player info of top scorer opponent player
      string newEnemyName = newOpponentPlayerInfo.username;// Get username of top scorer opponent player

      if (string.IsNullOrEmpty(newEnemyName) == false)// If enemy username is not null or empty
      {
        if (String.Equals(newEnemyName, enemyName) == false)// Set enemyName to newEnemyName only when there is new top scorer opponent
        {
          enemyName = newEnemyName;// Set enemyName to newEnemyName
          playerUI_InfoUpdater.UpdateOpponentInfo(newOpponentPlayerInfo);// Update opponent player UI info
        }
      }

      // If local player's score crosses highest final score among all opponents, we instantly call for GameOver for winner results instead of playing the remaining game
      if (matchData.is_my_game_over == true)// Stop the network if local player has won the game
      {
        isLocalPlayerWinner = true;// Set isLocalPlayerWinner true if local player has won the game
        StopNetwork();
      }
    }

    // Redirect to Native
    private void RedirectForResult()
    {
      communicateGamePlayStatusToJS.SendToJS_ToEndGame(); //Send End call to Native from Unity
    }
  }
}