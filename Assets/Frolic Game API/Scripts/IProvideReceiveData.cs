﻿
namespace FrolicGamesAPI
{
  // An interface to be implemented by a class which will provide local player data and receive match data 
  public interface IProvideReceiveData
  {
    PlayerData ProvideLocalPlayerData();// provide local player data
    void ReceiveMatchData(MatchData matchData);// receive match data
  }
}
