// Frolic Game API for Server API Calling
Documentation Link : https://gofynd.quip.com/UTjyA6B5VaAa/Frolic-Game-API-Documentation
/*
Steps/Points to consider while importing FrolicGameAPI package in a Unity project:

1. Once the package is imported, all of its content can be seen in Assets->Frolic Game API folder of Project Window.
2. Drag and drop the Bridge prefab (Assets->Frolic Game API→Prefabs of Project Window)  into the scene hierarchy. 
    1. *Note*: Make sure Bridge gameobject/prefab is always at a root/parent gameobject level in the scene hierarchy of the very first scene you want to load in the game. 
    2. In the inspector view of Bridge gameObject in the scene hierarchy, type the name of the scene inside "*scene To Load*" *field* of Bridge.cs script, which you want to *load next* after all Remote API Variables are set.
    3. In this package, by default, "*BridgeScene*" is the very first scene where we initialize all API variables (which are set Natively) and "*GameScene*" is set as the "*scene To Load*" field of Bridge.cs in the inspector.
3. Drag and drop the RemoteAPIVariablesProvider prefab (Assets->Frolic Game API→Prefabs of Project Window) into the scene hierachy. 
    1. *Note*: Make sure RemoteAPIVariablesProvider gameobject/prefab is always a root/parent gameobject in the scene hierarchy of the very first scene you want to load in the game.
    2. *Remember* : Bridge and RemoteAPIVariablesProvider prefabs should be in the very first Unity scene where you want to initialize/set Remote API Variables for the very first time.
4. Drag and drop the RemoteAPICaller prefab (Assets->Frolic Game API→Prefabs of Project Window) into the scene hierachy of a scene from where you desire to perform all API calls (mostly a gameplay scene).
    1. *Note*: In this package, by default GameScene is the scene from where are performing all API calls.
    2. All Remote API calls are invoked from RemoteAPICaller.cs script of RemoteAPICaller prefab/gameobject.
    3. In the inspector view of RemoteAPICaller gameObject in the scene hierarchy, set the *url* field for base server URL used for API calling,
    4. For setting API variables using Native values:
        1. Check the shouldInitializeViaNatively field in the inspector of RemoteAPICaller in the scene hierarchy to make it true.
        2. *Note*: By default this value is true, so API calls are invoked using API variables which are set Natively.
    5. For setting API variables using values set in Unity inspector:
        1. Uncheck the shouldInitializeViaNatively field in the inspector of RemoteAPICaller in the scene hierarchy to make it false.
        2. Manually set game_id and contest_id of heartBeatData field in the RemoteAPICaller inspector.
        3. Manually set the authToken and sessionId fields in the RemoteAPICaller inspector.
        4. *Note:* Now, all API calls are invoked using API variables set via Unity Inspector.
    6. Optional, messageFromServer_Text field can be used to display server response for all API calls that are made.
5. Supported API calls as of now in this package :
    1. HeartBeat API call :
        1. This api call is made in intervals and is used just to test the system.
    2. GameStop API : 
        1. This api call is made only when a game ends and to pass the final score and with a reason to end the game.
    3. Player-Data API : 
        1. This api call is made to send local player data and also to receive all players data.
6. RemoteAPICaller.cs class has a https://gofynd.quip.com/UTjyA6B5VaAa#DOQACAE5ob6() method with specific parameters, to be called explicitly, in order to launch/start the API calling.
7. Consuming the package :
    1. To configure calling the Server APIs, the Begin(*) method of RemoteAPICaller script should be called from a class that implements https://gofynd.quip.com/UTjyA6B5VaAa#DOQACAbK5iP interface.
    2. To make an explicit API call to send data to server, call  SendData() method of RemoteAPICaller script.
    3. To make an explicit API call to receive data from server, call  ReceiveData() method of RemoteAPICaller script.
    4. To make an explicit API call to stop the game, call  EndGame() method of RemoteAPICaller script.


// Sample code to get started
// SampleClass implements IProvideReceiveData interface
public class SampleClass : MonoBehaviour, IProvideReceiveData
{
    public RemoteAPICaller remoteAPICaller;
    
    void Start()
    {
        // remoteAPICaller.Begin(*) should be called only once in a game scene to configure API calling system
        
        // Different configuration options to choose from to initialize API calling system
        /*Option: 1*/ 
        remoteAPICaller.Begin(null, true);// To test that the system works, use this call to make heartbeat api calls in intervals
        
        /*Option: 2*/ 
        remoteAPICaller.Begin(this, false, true);// To make player_data SEND api calls in intervals [No heartbeat API calls]
        
        /*Option: 3*/ 
        remoteAPICaller.Begin(this, true, true, true);// To make player_data SEND and RECEIVE api calls in intervals [With heartbeat API calls]
    }
    
    // Called from RemoteAPICaller.cs to get local player data to be send to the server
    public PlayerData ProvideLocalPlayerData()
    {
        PlayerData playerData = new PlayerData();// PlayerData is a class from the imported packages FrolicGameAPI namespace
        playerData.score = 10;// sample score
        playerData.lives = 3;// sample lives
        playerData.time = 120;// sample time
    
        return playerData;// return PlayerData object
    }
    
    // Called from RemoteAPICaller.cs to receive all players data received from the server
    public void ReceiveAllPlayersData(AllPlayers allPlayers)
    {
        // Logs the first player data element in the received allPlayers.players_data collection
        Debug.Log($"My received data : score {allPlayers.players_data[0].data.score}, lives {allPlayers.players_data[0].data.lives}, time {allPlayers.players_data[0].data.time}");// log local player received score
    
        // Based on received players data we can perform UI and other game logics
    } 
}

*/

/*Steps/Points to consider in WebGL build index.html file inside script tag:
   
   Sample Code:

   const gameInstance = UnityLoader.instantiate("gameContainer", "Build/"Your Build Folder".json", {onProgress: UnityProgress});
        
    // Called from Native
   function SendToUnity(API_Variables){
        var jsonString = JSON.stringify(API_Variables);
        gameInstance.SendMessage("Bridge", "Send_APIVariables_ToUnity", jsonString);// Use this to initialize API Variables
    }
    // where, API_Variables is in JSON Format as follows : {"game_id" : "YOUR VALUE","contest_id" : "YOUR VALUE","session_id" : "YOUR VALUE","authToken" : "YOUR VALUE"};

    // Called from JS plugin
    function executeLoadGame(name, params={}) {
        console.log("Scene Loaded")
    }

    // Called from JS plugin
    function executeStopGame(name, params={}) {
        console.log("Stop Game")
      }

*/