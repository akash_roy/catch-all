namespace FrolicGamesAPI
{
    public class GameAnalyticsData
    {
        public string sessionId;
        public string score;
        public string total_currency;
        public string game_mode;
        public string contest_id;
        public string opponent_id;
        public string winning_amount;
        public string rank;
        public string total_adRequest;
        public string total_adWatch_count;
        public string total_reward_amount;
        public string log_type;
        public string msg;

        public GameAnalyticsData(string score = null, string totalCurrency = null,
            string gameMode = null, string contestId = null, string opponentId = null, string winningAmount = null,
            string rank = null, string totalAdRequest = null, string totalAdWatchCount = null, string totalRewardAmount = null, string logType = null,
           string sessionID = null, string msg = null)
        {
            this.sessionId = sessionID;
            this.score = score;
            this.total_currency = totalCurrency;
            this.game_mode = gameMode;
            this.contest_id = contestId;
            this.opponent_id = opponentId;
            this.winning_amount = winningAmount;
            this.rank = rank;
            this.total_adRequest = totalAdRequest;
            this.total_adWatch_count = totalAdWatchCount;
            this.total_reward_amount = totalRewardAmount;
            this.log_type = logType;
            this.msg = msg;
        }
    }
}
