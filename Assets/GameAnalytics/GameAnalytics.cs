﻿using System.Runtime.InteropServices;
using UnityEngine;

namespace FrolicGamesAPI
{
    public static class GameAnalytics
    {
    #region Events
        [DllImport("__Internal")]
        private static extern void SendEventInitialize(string apiKey, string apiUrl);

        [DllImport("__Internal")]
        private static extern void SendGameAnalytics(string eventName, string userId, string gameId, string propertyData);

        [DllImport("__Internal")]
        private static extern void SetIdentity(string id, string emailId, string userName, bool enableAllDestination);
    #endregion


    #region Function
        /// <summary>
        /// Initialize Event system
        /// </summary>
        /// <param name="api_Key">API key for game</param>
        /// <param name="api_Url"> API end point</param>
        public static void OnEventInitialize(string eventKey, string eventURL)
        {
#if !UNITY_EDITOR && UNITY_WEBGL
            string apiKey = eventKey;
            string apiUrl = eventURL;
            SendEventInitialize(apiKey, apiUrl);
#endif
        }

        /// <summary>
        /// Game Event
        /// </summary>
        /// <param name="event_Name">User name used to store in Database</param>
        /// <param name="user_Id">Unique user id </param>
        /// <param name="game_Id">Game Id </param>
        /// <param name="property_Data">Game related data</param>
        public static void OnGameAnalytics(string event_Name, string user_Id, string game_Id, GameAnalyticsData property_Data)
        {
#if !UNITY_EDITOR && UNITY_WEBGL
            if (GameModeProvider.modeType == GameModeType.None || GameModeProvider.modeType == GameModeType.Singleplayer)
                return;

            string eventName = event_Name;
            string userId = user_Id;
            string gameId = game_Id;
            string propertyData = property_Data != null ? JsonUtility.ToJson(property_Data) : "null";
            SendGameAnalytics(eventName, userId, gameId, propertyData);
#endif
        }
        #endregion
    }

    public struct GameEventConst
    {
        public const string nativeHandshake = "native_handshake"; // First calls from Unity to Native
        public const string pingStart ="ping_start"; //First ping to server
        public const string pongReceived ="pong_received"; // Pong received from server
        public const string gameLaunch = "game_launch"; // Unity Instance Loading point
        public const string gameStart = "game_start"; //Game Scene Loaded 
        public const string gameOver = "game_over"; // Game over 
        public const string gameAdwatch = "game_adwatch"; // On Ad watch 
        public const string gameDebug = "game_debug"; // Debug logs
    }
}
