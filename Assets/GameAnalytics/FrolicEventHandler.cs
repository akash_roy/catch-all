using UnityEngine;

namespace FrolicGamesAPI
{
  public class FrolicEventHandler : MonoBehaviour
  {
    private static FrolicEventHandler instance;
    public static FrolicEventHandler Instance
    {
      get
      {
        if (instance == null)
        {
          GameObject gObj = new GameObject("FrolicEventHandler");
          instance = gObj.AddComponent<FrolicEventHandler>();
          DontDestroyOnLoad(gObj);
        }
        return instance;
      }
    }

    void Awake()
    {
      Debug.Log("Awake FrolicEventHandler");
    }

    // Native handshake form Unity is initilise
    public void OnNativeHandShake()
    {
      if (UserInfoData.pInstance != null)
      {
        UserInfo userInfo = UserInfoData.pInstance.userInfo;
        GameAnalyticsData gameAnalyticsData = new GameAnalyticsData()
        {
          sessionId = userInfo.sessionId
        };
        GameAnalytics.OnGameAnalytics(GameEventConst.nativeHandshake, userInfo.id, userInfo.gameId, gameAnalyticsData);
      }
    }

    // Ping started from game
    public void OnPingStart()
    {
      if (UserInfoData.pInstance != null)
      {
        UserInfo userInfo = UserInfoData.pInstance.userInfo;
        GameAnalyticsData gameAnalyticsData = new GameAnalyticsData()
        {
          sessionId = userInfo.sessionId
        };
        GameAnalytics.OnGameAnalytics(GameEventConst.pingStart, userInfo.id, userInfo.gameId, gameAnalyticsData);
      }
    }

    // Pong received to game
    public void OnPongReceived()
    {
      if (UserInfoData.pInstance != null)
      {
        UserInfo userInfo = UserInfoData.pInstance.userInfo;
        GameAnalyticsData gameAnalyticsData = new GameAnalyticsData()
        {
          sessionId = userInfo.sessionId
        };
        GameAnalytics.OnGameAnalytics(GameEventConst.pongReceived, userInfo.id, userInfo.gameId, gameAnalyticsData);
      }
    }

    // Game Launch triggred 
    public void OnGameLaunch()
    {
      if (UserInfoData.pInstance != null)
      {
        UserInfo userInfo = UserInfoData.pInstance.userInfo;
        GameAnalyticsData gameAnalyticsData = new GameAnalyticsData()
        {
          sessionId = userInfo.sessionId,
          contest_id = userInfo.contestId,
          game_mode = userInfo.gameMode
        };
        GameAnalytics.OnGameAnalytics(GameEventConst.gameLaunch, userInfo.id, userInfo.gameId, gameAnalyticsData);
      }
    }

    // Game Start event 
    public void OnGameStart()
    {
      if (UserInfoData.pInstance != null)
      {
        UserInfo userInfo = UserInfoData.pInstance.userInfo;
        GameAnalyticsData gameAnalyticsData = new GameAnalyticsData()
        {
          contest_id = userInfo.contestId,
          game_mode = userInfo.gameMode
        };

        GameAnalytics.OnGameAnalytics(GameEventConst.gameStart, userInfo.id, userInfo.gameId, gameAnalyticsData);
      }
    }

    //Game over event
    public void OnGameOver(string gameScore)
    {
      if (UserInfoData.pInstance != null)
      {
        UserInfo userInfo = UserInfoData.pInstance.userInfo;
        GameAnalyticsData gameAnalyticsData = new GameAnalyticsData()
        {
          contest_id = userInfo.contestId,
          game_mode = userInfo.gameMode,
          score = gameScore
        };

        GameAnalytics.OnGameAnalytics(GameEventConst.gameOver, userInfo.id, userInfo.gameId, gameAnalyticsData);
      }
    }
  }
}
