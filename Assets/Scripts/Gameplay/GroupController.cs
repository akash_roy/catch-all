using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroupController : MonoBehaviour
{
    [SerializeField] private float speed = 5f;
    [SerializeField] private GameObject[] levelObjects;
    [SerializeField] private Transform groupOrigin;
    [SerializeField] private Transform groupBoundary;

    private bool canMove = true;

    void Update()
    {
        if(canMove)
        {
            float xPos = transform.position.x;
            float yPos = transform.position.y - (speed * Time.deltaTime);
            transform.position = new Vector2(xPos, yPos);
        }
    }

    private void OnEnable()
    {
        foreach(GameObject obj in levelObjects)
        {
            obj.SetActive(true);
        }
    }

    public void EnableMovement(bool arg)
    {
        canMove = arg;
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.tag == MyTags.COLLECTOR_TAG)
        {
            Vector2 pos = new Vector2(groupOrigin.position.x, (groupBoundary.position.y - groupOrigin.position.y) / 2);
            SpawnController.Instance.SpawnBlockGroup(pos);
            gameObject.SetActive(false);
        }
    }
}
