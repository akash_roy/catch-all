using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletController : MonoBehaviour
{
    private Rigidbody2D myBody;
    //[SerializeField] private float speed = 2.5f;

    void Awake()
    {
        myBody = GetComponent<Rigidbody2D>();
    }

    public void Move(float strength)
    {
        myBody.AddForce(transform.up.normalized * strength);

        Invoke("DeactivateGameObject", 1f);
    }

    void DeactivateGameObject()
    {
        gameObject.SetActive(false);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == MyTags.HAZARD_TAG)
        {
            GameObject explosion = ObjectPooler.Instance.GetPooledObject("ExplosionObject");
            explosion.transform.position = collision.gameObject.transform.position;
            explosion.SetActive(true);
            
            SoundManager.PlaySound(SoundManager.Sound.HitHazard);

            collision.gameObject.SetActive(false);

            gameObject.SetActive(false);
        }
        if (collision.gameObject.tag == MyTags.OBSTACLE_TAG)
        {
            SoundManager.PlaySound(SoundManager.Sound.HitObstacle);
            
            gameObject.SetActive(false);
        }
    }
}