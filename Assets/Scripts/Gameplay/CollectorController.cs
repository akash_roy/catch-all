﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CollectorController : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D target)
    {
        if(target.tag == MyTags.OBSTACLE_TAG || target.tag == MyTags.HAZARD_TAG)
        {
            target.gameObject.SetActive(false);
        }
        if(target.tag == MyTags.COLLECTABLE_TAG)
        {   
            target.gameObject.SetActive(false);
        }
    }
}
