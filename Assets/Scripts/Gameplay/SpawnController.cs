﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnController : MonoBehaviour
{
    private float minX = -2.55f;
    private float maxX = 2.55f;
    private bool canLevelMove = true;

    public static SpawnController Instance { get; private set; }

    [SerializeField] private bool isSpawning = true;
    [SerializeField] private bool spawnRandomly = true;

    public GameObject[] levelGroups;
    //public GameObject[] hazardPrefabs;
    //public GameObject[] collectablePrefabs;
    //public GameObject[] obstaclePrefabs;

    private void Awake()
    {
        Instance = this;
    }

    void Start()
    {
        //StartCoroutine(SpawnElements());
    }
    
    public void SpawnBlockGroup(Vector2 pos)
    {
        int gIndex = Random.Range(0, levelGroups.Length);
        GameObject temp = levelGroups[gIndex];

        GameObject pooledObject = ObjectPooler.Instance.GetPooledObject(temp.name);

        pooledObject.transform.position = pos;
        pooledObject.transform.rotation = Quaternion.identity;

        pooledObject.GetComponent<GroupController>().EnableMovement(canLevelMove);
        pooledObject.SetActive(true);
    }

    #region Random Object Spawn
    /*
    public void EnableSpawn(bool val)
    {
        isSpawning = val;
        if (!val)
        {
            StopCoroutine(SpawnElements());
        }
        else
        {
            StartCoroutine(SpawnElements());
        }
    }
    
    /*
    private IEnumerator SpawnElements()
    {
        yield return new WaitForSeconds(Random.Range(0f, 0.5f));

        if (isSpawning)
        {
            GameObject temp;

            int hIndex = Random.Range(0, hazardPrefabs.Length);
            int cIndex = Random.Range(0, collectablePrefabs.Length);
            int oIndex = Random.Range(0, obstaclePrefabs.Length);
            int spawnType = Random.Range(0, 3);

            if (spawnType == 0)
            {
                temp = hazardPrefabs[hIndex];
            }
            else if (spawnType == 1)
            {
                temp = collectablePrefabs[cIndex];
            }
            else //if(spawnType == 2)
            {
                temp = obstaclePrefabs[oIndex];
            }

            GameObject pooledObejct = ObjectPooler.Instance.GetPooledObject(temp.name);

            pooledObejct.transform.position = new Vector2(Random.Range(minX, maxX), transform.position.y);
            pooledObejct.transform.rotation = Quaternion.identity;

            float waitForNext = Random.Range(0.25f, 0.5f);

            yield return new WaitForSeconds(waitForNext);

            pooledObejct.SetActive(true);

            SoundManager.PlaySound(SoundManager.Sound.SpawnHazard);

            StartCoroutine(SpawnElements());
        }
    }
    */
    /*
    public void EnableLevelMovement(bool arg)
    {
        canLevelMove = arg;
    }
    */
    #endregion

}
