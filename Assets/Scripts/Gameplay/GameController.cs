using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using FrolicGamesAPI;
using System;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour , IProvideFinalScore
{
    public static Action onScoreUpdate;
    private int score = 0;
    public int getScore { get { return score; } }
    private PlayerController playerController;
    private GameObject[] enemies;
    private Vector3 initialPosition;

    [SerializeField] private GameObject spawner;
    [SerializeField] private GameObject gameOverPanel;
    [SerializeField] private GameObject player;
    [SerializeField] private LevelLoader levelLoader;

    [Header("Multi-player UI")]
    [SerializeField] private GameObject multiPlayerPanel;
    [SerializeField] private Text mp_localUsernameText;
    [SerializeField] private Text mp_localScoreText;
    [SerializeField] private Text mp_enemyUsernameText;
    [SerializeField] private Text mp_enemyScoreText;
    [SerializeField] private Text mp_livesText;
    [SerializeField] private Text mp_timerText;
    
    [Header("Single-player UI")]
    [SerializeField] private GameObject singlePlayerPanel;
    [SerializeField] private Text sp_localUsernameText;
    [SerializeField] private Text sp_localScoreText;
    [SerializeField] private Text sp_livesText;
    [SerializeField] private Text sp_timerText;

    [Space]
    [SerializeField] GameObject singleplayer_heartPanel;
    [SerializeField] GameObject multiplayer_heartPanel;
    [SerializeField] Button shootButton;
    [SerializeField] Button soundButton;
    [SerializeField] Button restartButton;
    [SerializeField] private Animator shootButtonAnimator;
    [SerializeField] private Animator soundButtonAnimator;
    [SerializeField] Text finalScoreText;

    [Space]
    public bool isGameOver = false;

    public static GameController Instance { get; private set; }

    private void Awake()
    {
        Instance = this;
        isGameOver = false;
        playerController = player.GetComponent<PlayerController>();
        PlayerController.OnLifeReduction += ResetLevel;

        shootButton.onClick.AddListener(OnClickShootButton);
        soundButton.onClick.AddListener(OnClickSoundButton);
        restartButton.onClick.AddListener(OnClickRestartButton);

        initialPosition = player.transform.position;
    }

    private void Start()
    {
        NetworkManager.Instance.StartNetwork(this);
        FrolicEventHandler.Instance.OnGameStart();
        NetworkManager.Instance.onGameOver += OnDeclareGameOver;
        CountDownTimer.Instance.getCurrentTime += UpdateCountdownTime;
        PlayerUI_InfoUpdater.Instance.getOpponentScore += OnOpponentScoreUpdate;
        PlayerUI_InfoUpdater.Instance.getLocalPlayer_Name += OnLocalPlayerNameUpdate;
        PlayerUI_InfoUpdater.Instance.getOpponentPlayer_Name += OnOpponentNameUpdate;

        onScoreUpdate += IncreaseScore;

        if (GameModeProvider.modeType == GameModeType.Singleplayer)
        {
            singlePlayerPanel.SetActive(true);
            multiPlayerPanel.SetActive(false);
        }
        else
        {
            singlePlayerPanel.SetActive(false);
            multiPlayerPanel.SetActive(true);
        }
    }

    private void OnOpponentNameUpdate(string obj)
    {
        //update oppponent name
        if (GameModeProvider.modeType == GameModeType.Multiplayer)
        {
            mp_enemyUsernameText.text = obj;
        }
    }

    private void OnLocalPlayerNameUpdate(string obj)
    {
        //upadte local player name
        if (GameModeProvider.modeType == GameModeType.Singleplayer)
        {
            sp_localUsernameText.text = obj;
        }
        else
        {
            mp_localUsernameText.text = obj;
        }
    }

    private void UpdateCountdownTime(string timeArg)
    {
        //update timer value
        if (GameModeProvider.modeType == GameModeType.Singleplayer)
        {
            sp_timerText.text = timeArg;
        }
        else
        {
            mp_timerText.text = timeArg;
        }
    }

    private void OnOpponentScoreUpdate(int scoreArg)
    {
        //update enemy score text
        if (GameModeProvider.modeType == GameModeType.Multiplayer)
        {
            mp_enemyScoreText.text = scoreArg.ToString();
        }
    }

    internal void OnDeclareGameOver()
    {
        if(isGameOver==false)
        {
            isGameOver = true;
            FrolicEventHandler.Instance.OnGameOver(getScore.ToString());
            Debug.Log("Unity Message: Game Over");
            NetworkManager.Instance.StopNetwork();
            if(GameModeProvider.modeType == GameModeType.Singleplayer)
            {
                GameOver();
            }
        }
    }

    private void OnDisable()
    {
        PlayerController.OnLifeReduction -= ResetLevel;
    }

    private void OnClickShootButton()
    {
        shootButtonAnimator.Play("Button_Click");
        playerController.FireCannon();
    }

    private void OnClickSoundButton()
    {
        soundButtonAnimator.Play("Button_Click");
    }

    private void OnClickRestartButton()
    {
        SoundManager.PlaySound(SoundManager.Sound.buttonClick);
        RestartGame();
    }

    private void IncreaseScore()
    {
        score++;
        if (GameModeProvider.modeType == GameModeType.Singleplayer)
        {
            sp_localScoreText.text = score.ToString();
        }
        else
        {
            mp_localScoreText.text = score.ToString();
        }
    }

    private void ResetLevel()
    {
        if (GameModeProvider.modeType == GameModeType.Singleplayer)
        {
            //sp_livesText.text = playerController.LifeCount.ToString();
        }
        else
        {
            //mp_livesText.text = playerController.LifeCount.ToString();
        }

        ReduceLife();
        StartCoroutine(WaitResetLevel(3));
    }

    private void RestartGame()
    {
        Time.timeScale = 1.0f;
        isGameOver = false;
        
        string sceneName = SceneManager.GetActiveScene().name;
        SceneManager.LoadScene(sceneName);
    }

    private void GameOver()
    {
        Time.timeScale = 0f;
        isGameOver = true;
        SoundManager.PlaySound(SoundManager.Sound.GameOver);

        GameObject[] enemiesArray = GameObject.FindGameObjectsWithTag("Hazard");
        enemies = enemiesArray;

        //spawner.GetComponent<SpawnController>().EnableSpawn(false);   //for random object spawn

        gameOverPanel.SetActive(true);
        finalScoreText.text = score.ToString();
    }

    private IEnumerator WaitResetLevel(float wait)
    {
        MoveLevel(false);
        //SpawnController.Instance.EnableSpawn(false);
        playerController.EnableMovement(false);
        player.gameObject.SetActive(false);

        yield return new WaitForSecondsRealtime(wait);

        player.transform.position = initialPosition;
        player.gameObject.SetActive(true);
        playerController.EnableMovement(true);
        //SpawnController.Instance.EnableSpawn(true);
        MoveLevel(true);
    }

    public int GetLocalPlayerFinalScore()
    {
        return getScore;
    }

    private void MoveLevel(bool arg)
    {
        //SpawnController.Instance.EnableLevelMovement(arg);
        
        GameObject[] levelGroups = GameObject.FindGameObjectsWithTag(MyTags.OBSTACLE_GROUP_TAG);

        for(int i=0; i<levelGroups.Length; i++)
        {
            levelGroups[i].GetComponent<GroupController>().EnableMovement(arg);
        }
    }

    private void ReduceLife()
    {
        if (GameModeProvider.modeType == GameModeType.Singleplayer)
        {
            singleplayer_heartPanel.transform.GetChild(playerController.LifeCount).GetChild(0).gameObject.SetActive(false);
        }
        else if (GameModeProvider.modeType == GameModeType.Multiplayer)
        {
            multiplayer_heartPanel.transform.GetChild(playerController.LifeCount).GetChild(0).gameObject.SetActive(false);
        }
    }
}
