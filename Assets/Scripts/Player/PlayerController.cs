﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public LevelLoader levelLoader;
    
    [SerializeField] private GameObject cannon;
    [SerializeField] private GameObject cannonBallPrefab;
    [SerializeField] private int maxLives;
    [SerializeField] private float shootingForce = 1000f;
    [SerializeField] private float bulletRecovery = 0.25f;
    
    private bool canShoot = true;
    public bool CanShoot { get { return canShoot; } }

    private int lifeCount;
    public int LifeCount { get { return lifeCount; } }

    private bool canMove = true;
    public bool CanMove { get { return canMove; } }
    
    private PlayerMovementController movementController;

    public static event Action OnLifeReduction;

    private void Awake()
    {
        SoundManager.Initialize();
        lifeCount = maxLives;
        movementController = GetComponent<PlayerMovementController>();
    }

    private void Update()
    {
        if (Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.LeftArrow) || Input.GetKey(KeyCode.RightArrow))
        {
            SoundManager.PlaySound(SoundManager.Sound.PlayerMove);
        }
        
        if (Input.GetKey(KeyCode.Space))
        {
            FireCannon();
        }

        movementController.MoveByKey();

        movementController.MoveByTouch();
    }

    public void EnableMovement(bool val)
    {
        canMove = val;
    }

    private void OnTriggerEnter2D(Collider2D target)
    {
        if(target.tag == MyTags.HAZARD_TAG || target.tag == MyTags.OBSTACLE_TAG)
        {
            lifeCount--;
            OnLifeReduction?.Invoke();
            
            if(target.tag == MyTags.HAZARD_TAG)
            {
                SoundManager.PlaySound(SoundManager.Sound.HitHazard);
            }

            if(target.tag == MyTags.OBSTACLE_TAG)
            {
                SoundManager.PlaySound(SoundManager.Sound.HitObstacle);
            }
            
            SoundManager.PlaySound(SoundManager.Sound.PlayerDie);
            
            if (lifeCount == 0)
            {
                GameController.Instance.OnDeclareGameOver();
            }

            GameObject explosion = ObjectPooler.Instance.GetPooledObject("ExplosionObject");
            explosion.transform.position = gameObject.transform.position;
            explosion.SetActive(true);

            target.gameObject.SetActive(false);
        }

        if (target.tag == MyTags.COLLECTABLE_TAG)
        {
            SoundManager.PlaySound(SoundManager.Sound.HitCollectable);
            GameController.onScoreUpdate?.Invoke();
            target.gameObject.SetActive(false);
        }
    }

    public void FireCannon()
    {
        if (Time.timeScale != 0)
        {
            if (CanShoot)
            {
                GameObject pooledBullet = ObjectPooler.Instance.GetPooledObject("CannonBall");

                pooledBullet.transform.position = cannon.transform.position;
                pooledBullet.transform.rotation = Quaternion.identity;
                pooledBullet.SetActive(true);

                pooledBullet.GetComponent<BulletController>().Move(shootingForce);
                //shootFX.Play();

                canShoot = false;

                Invoke("ReloadCannon", bulletRecovery);
            }
        }
    }

    public void ReloadCannon()
    {
        canShoot = true;
    }
}
