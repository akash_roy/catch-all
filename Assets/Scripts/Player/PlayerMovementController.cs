using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class PlayerMovementController : MonoBehaviour
{
    private GameController gameController;
    private PlayerController playerController;
    private Rigidbody2D rb;
    private Camera mainCamera;
    private Collider2D col;

    private bool isMoving;
    private float xMin, xMax, yMin, yMax;

    private Vector2 shipPosV;
    private Vector2 screenBounds;
    private Vector3 touchPos;
    private Vector3 direction;

    [SerializeField] private GameObject shipSprite;
    [SerializeField] private Vector2 offSet = new Vector2(0f, 0f);
    [SerializeField] private float movementSpeed = 1f;
    [SerializeField] private float tiltAngle = 3f;
    [SerializeField] private float tiltSmoothness = 5f;

    private void Awake()
    {
        mainCamera = Camera.main;
        gameController = GameController.Instance;
        playerController = GetComponent<PlayerController>();

        rb = GetComponent<Rigidbody2D>();
        col = GetComponent<Collider2D>();
    }

    private void Start()
    {
        shipPosV = transform.position;
        screenBounds = mainCamera.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height, mainCamera.transform.position.z));

        xMin = -screenBounds.x;
        xMax = screenBounds.x;
        yMin = -screenBounds.y;
        yMax = screenBounds.y;
    }

    public void MoveByTouch()
    {
        if (Input.GetMouseButtonDown(0) && !IsPointerOverUIObject())
        {
            isMoving = true;
        }

        if (Input.GetMouseButtonUp(0) && !IsPointerOverUIObject())
        {
            isMoving = false;
            rb.velocity = Vector2.zero;
        }

        Vector2 currentPosition = transform.position;   //current position of gameObject

        if (isMoving)
        {
            Vector2 MousePos = Input.mousePosition;

            float ratioY = MousePos.y / Screen.height;
            float ratioX = MousePos.x / Screen.width;

            if (ratioY < yMax && ratioX < xMax && ratioY > yMin && ratioX > xMin)
            {
                touchPos = Camera.main.ScreenToWorldPoint(MousePos);
                touchPos.z = 0f;

                direction = (touchPos - rb.transform.position);

                Quaternion tilt;

                if(direction.x > 0)
                {
                    tilt = Quaternion.Euler(0f, 0f, -tiltAngle);
                    shipSprite.transform.rotation = Quaternion.Slerp(shipSprite.transform.rotation, tilt, Time.deltaTime * tiltSmoothness);
                }
                else if(direction.x < 0)
                {
                    tilt = Quaternion.Euler(0f, 0f, tiltAngle);
                    shipSprite.transform.rotation = Quaternion.Slerp(shipSprite.transform.rotation, tilt, Time.deltaTime * tiltSmoothness);
                }
                else
                {
                    tilt = Quaternion.Euler(0f, 0f, 0f);
                    shipSprite.transform.rotation = Quaternion.Slerp(shipSprite.transform.rotation, tilt, Time.deltaTime * tiltSmoothness);
                }

                rb.velocity = new Vector2(direction.x, direction.y + offSet.y) * movementSpeed;
            }
            else
            {
                rb.velocity = Vector2.zero;
            }

        }

        if (currentPosition.x < xMin)
        {
            currentPosition.x = xMin;
        }

        if (currentPosition.x > xMax)
        {
            currentPosition.x = xMax;
        }

        if (currentPosition.y < shipPosV.y)
        {
            currentPosition.y = shipPosV.y;
        }

        if (currentPosition.y > shipPosV.y)
        {
            currentPosition.y = shipPosV.y;
        }

        transform.position = currentPosition;           //updating gameObject's new positio
    }

    public void MoveByKey()
    {
        float h = Input.GetAxis("Horizontal");
        float v = Input.GetAxis("Vertical");

        Vector2 currentPosition = transform.position;   //current position of gameObject

        if (playerController.CanMove)
        {
            if (h > 0)                                  //moving towards right
            {
                currentPosition.x += movementSpeed * Time.deltaTime;
            }
            else if (h < 0)                             //moving towards left side
            {
                currentPosition.x -= movementSpeed * Time.deltaTime;
            }

            if (v > 0)                                  //moving towards up
            {
                currentPosition.y += movementSpeed * Time.deltaTime;
            }
            else if (v < 0)                             //moving towards down
            {
                currentPosition.y -= movementSpeed * Time.deltaTime;
            }

            if (currentPosition.x < xMin)
            {
                currentPosition.x = xMin;
            }

            if (currentPosition.x > xMax)
            {
                currentPosition.x = xMax;
            }

            if (currentPosition.y < yMin)
            {
                currentPosition.y = yMin;
            }

            if (currentPosition.y > yMax)
            {
                currentPosition.y = yMax;
            }
        }

        transform.position = currentPosition;           //updating gameObject's new position
    }

    private bool IsPointerOverUIObject()
    {
        PointerEventData eventDataCurrentPosition = new PointerEventData(EventSystem.current);
        eventDataCurrentPosition.position = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
        List<RaycastResult> results = new List<RaycastResult>();
        EventSystem.current.RaycastAll(eventDataCurrentPosition, results);
        return results.Count > 0;
    }
}