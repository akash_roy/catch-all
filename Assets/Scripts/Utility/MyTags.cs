using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class MyTags
{
    public static string OBSTACLE_TAG = "Obstacle"; 
    public static string HAZARD_TAG = "Hazard"; 
    public static string COLLECTABLE_TAG = "Collectable"; 
    public static string OBSTACLE_GROUP_TAG = "ObstacleGroup"; 
    public static string COLLECTOR_TAG = "Collector"; 
}
