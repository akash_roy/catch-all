using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PrefabBehaviour : MonoBehaviour
{
    private Animator animator;

    private void Awake()
    {
        animator = GetComponent<Animator>();
    }

    private void OnEnable()
    {
        float delay = Random.Range(0f, 1f);
        animator.Play("LoopState1", 0, delay);
        //Debug.Log(gameObject.name + " spawned with delay:" + delay);
    }
}
